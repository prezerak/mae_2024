import java.util.Scanner;
import java.util.*;
import java.util.stream.*;

public class StudentController {

	public Student Add() {
		Scanner sc = new Scanner(System.in);

		String fname = this.readName("First name:", sc);
		String lname = this.readName("Last name:", sc);
		int am = this.readAM(sc);

		App.db.add(new Student(fname, lname, am));
		return (Student) App.db.toArray()[App.db.size() - 1];
	}
	
	public Student Search() {
		Scanner sc = new Scanner(System.in);
		int am = this.readAM(sc);				
		
		if (App.db.stream()
			   .anyMatch(x -> x.getStudentid() == am))
		{
			return App.db.stream()
			.filter(x -> x.getStudentid() == am)
			.findFirst().get();
		}
		
		return null;
				
		
	}
	
	public Student Update(Student s) {
		Student oldStudent;
		
		if (App.db.stream()
				   .anyMatch(x -> x.getStudentid() == s.getStudentid()))
			{
				oldStudent =  App.db.stream()
						.filter(x -> x.getStudentid() == s.getStudentid())
						.findFirst().get();
			}
		
		Student newRecord = new Student(s);	
		App.db.remove(oldStudent);
		App.db.add(newRecord);
	}
	
	
	public void Delete() throws NotFoundException {			
		Student s = this.Search();
		try {
			if (s == null) {
				throw new NotFoundException();			
			}		
		} catch(NotFoundException e) {
			System.out.println("Student not found");
			return;
		} 
		
		App.db.remove(s);
	}

	public String readName(String prompt, Scanner sc) {
		boolean invalid = true;
		String name = "";

		while (invalid) {
			System.out.print(prompt);
			name = sc.nextLine();
			for (char c : name.toCharArray()) {
				if (!Character.isLetter(c)) {
					invalid = true;
					break;
				}
				else {
					invalid = false;
				}
			}
			
		}
		return name;
	}

	/*
	 * Originally written by I. Xera on Apr. 15 2024
	 */
	public int readAM(Scanner sc) {

		int am = 0;

		while (true) {
			System.out.print("AM:");
			if (sc.hasNextInt()) {
				am = sc.nextInt();
				if (am > 0) {
					return am;
				}
			} else {
				sc.nextLine();
			}

			System.out.println("Invalid input, please try again!");
		}
	}
	
	class NotFoundException extends Exception {
		
	}
}
