
public class Student {
	
	private String firstname;
	private String lastname;
	private int studentid;
	
	//private double salary;

	public Student(String firstname, String lastname, int studentid) {	
		this.firstname = firstname;
		this.lastname = lastname;
		this.studentid = studentid;
	}

	public Student() {
		// TODO Auto-generated constructor stub
	}
	
	public Student(String firstname, String lastname) {		
		this.firstname = firstname;
		this.lastname = lastname;
	}
	
	public Student(Student s) {
		this.firstname = s.firstname;
		this.lastname = s.lastname;
	}

	

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public int getStudentid() {
		return studentid;
	}

	public void setStudentid(int studentid) {
		this.studentid = studentid;
	}

	

	public void printEverything() {
		System.out.println(this.firstname);
		System.out.println(this.lastname);
		System.out.println(this.studentid);
	}
	
	

}
