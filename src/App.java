import java.util.*;


public class App {
	
static ArrayList<Student> db = new ArrayList<Student>();	

	public static void main(String[] args) {
		String [] options = new String[5];
		
		options[0] = "1. Add Student";
		options[1] = "2. Find Student";
		options[2] = "3. Update Student";
		options[3] = "4. Delete Student";
		options[4] = "5. Exit";
		
		MainMenu menu = new MainMenu(options);
		StudentController stdSc = new StudentController();
		int selection = 0;
		do {
			menu.show();	
			selection = menu.getSelection();
			switch (selection) {
				case 1:
					stdSc.Add();
					break;
				case 2:
					var s = stdSc.Search();
					if (s != null) {
						s.printEverything();
					}
					else {
						System.out.println("Not found");
					}
					break;
				case 4:
					stdSc.Delete();
					break;
			}
		} while(selection != 5);
		System.out.println("APP EXITED!!!!!");
	}

}
