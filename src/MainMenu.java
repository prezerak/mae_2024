import java.util.Scanner;

public class MainMenu {
	private final String[] _options;

	public MainMenu(String[] options) {
		this._options = options;
	}

	public void show() {
		if (_options == null) {
			return;
		}

		for (int i = 0; i < 50; i++) {
			System.out.println();
		}

		// for(int i=0; i < _options.length; i++) {
		for (String s : _options) {
			System.out.println(s);
		}
	}

	public int getSelection() {

		
		while (true) {
			System.out.print("Please make a selection:");
			Scanner sc = new Scanner(System.in);

			if (sc.hasNextInt()) {
				return  sc.nextInt();								
			} else {
				System.out.println("Wrong input!!!");
			}
		}
	}
}
